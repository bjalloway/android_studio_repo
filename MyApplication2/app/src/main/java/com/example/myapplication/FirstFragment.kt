package com.example.myapplication

import android.app.PendingIntent.getActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.myapplication.MainActivity.Communicator
import com.google.android.material.internal.ContextUtils.getActivity
import kotlinx.android.synthetic.main.fragment_first.*



/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {



    var comm: MainActivity.Communicator?
        get() = null
        set(value) = TODO()
    val f1_info = Info();
    var resultURL = "";

    override fun onCreateView(

            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {



        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        view.findViewById<Button>(R.id.SearchButton).setOnClickListener {

            //Apply values to all non-dropdown options
            f1_info.item = itemInput.text.toString();
            f1_info.levelMin = minLevelInput.text.toString();
            f1_info.levelMax = maxLevelInput.text.toString();
            f1_info.WritVoucherMin = minWritVoucherInput.text.toString();
            f1_info.WritVoucherMax = maxWritVoucherInput.text.toString();
            f1_info.minQuantity = minAmountInput.text.toString();
            f1_info.maxQuantity = maxAmountInput.text.toString();
            f1_info.priceMin = minPriceInput.text.toString();
            f1_info.priceMax = maxPriceInput.text.toString();

            //create result url
            f1_info.resultURL= f1_info.createResultURL(f1_info.SearchType, f1_info.item, f1_info.category, f1_info.type,f1_info.trait,f1_info.quality,f1_info.levelMin,
                                                f1_info.WritVoucherMin,f1_info.minQuantity,f1_info.maxQuantity,f1_info.priceMin, f1_info.priceMax,f1_info.champion,f1_info.levelMax,f1_info.WritVoucherMax);






            comm?.passDataCom(resultURL);
            findNavController()
                .navigate(R.id.action_FirstFragment_to_SecondFragment);

        }

    }

    fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long
    ) {
        //whenevr things are selected, update the variables in the Info object (consider putting this in the onClick fo rthe search button for optimization?)
        f1_info.SearchType = searchTypeSpinner[searchTypeSpinner.selectedItemPosition].toString();
        f1_info.category = categoryInput[categoryInput.selectedItemPosition].toString();
        f1_info.quality = qualityInput[qualityInput.selectedItemPosition].toString();
        f1_info.trait = traitInput[traitInput.selectedItemPosition].toString();
        f1_info.type = typeInput[typeInput.selectedItemPosition].toString();
    }










}

