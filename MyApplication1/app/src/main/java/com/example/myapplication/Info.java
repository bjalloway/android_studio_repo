package com.example.myapplication;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.xpath.XPathExpression;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import static org.w3c.dom.Node.ELEMENT_NODE;

public class Info implements Serializable
{

    //Data needed from user for lookup
    //Listing vs Price Check
    public String SearchType = "";
    //Item to be searched
    public String item = "";
    //Item Category
    public String category = "";
    //Item Type
    public String type = "";
    //Item Trait
    public String trait = "";
    //Item Quality
    public String quality = "";
    //Item Level range
    public String levelMin = "";
    public String levelMax = "";
    //Writ Voucher value if applicable
    public String WritVoucherMin = "";
    public String WritVoucherMax = "";
    //Quantity of item in listing
    public String minQuantity = "";
    public String maxQuantity ="";
    //Unit price of item
    public String priceMin = "";
    public String priceMax = "";
    //Champion Level
    public String champion = "false";

    //Holders for all dropdown labels and their corresponding index so they can be found dynamically at runtime
    //Create holders for each label and index
    Hashtable<String, String> categories = new Hashtable<String, String>();
    Hashtable<String, String> types = new Hashtable<String, String>();
    Hashtable<String, String> qualities = new Hashtable<String, String>();
    Hashtable<String, String> traits = new Hashtable<String, String>();

    //Create holder for results
    public BreakIterator content;

    public String resultURL = "";

    public ArrayList returnValues = new ArrayList();


    Info()
    {
    //default constructor with Null values
    }
    public void getXML(String url) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        URL resultURL = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) resultURL.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("ACCEPT", "application/xml");
        InputStream xml = conn.getInputStream();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(xml);

        XPathFactory pathFactory = XPathFactory.newInstance();
        XPath path = pathFactory.newXPath();
        XPathExpression expression;
        expression = (XPathExpression) path.compile("/result/checkid");
        NodeList nodeList = (NodeList) expression.evaluate(document, ELEMENT_NODE, XPathConstants.NODESET);

        String checkids[] = getNodeValue(nodeList);


        for (String checkid : checkids) {
            returnValues.add(checkid);
            System.out.print(checkid + ", ");
        }
        conn.disconnect();



    }

    static String[] getNodeValue(NodeList nodes) {
        String checkIds[] = new String[nodes.getLength()];
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            checkIds[i] = node.getTextContent();
        }
        return checkIds;
    }
    public void get_options()
    {
        //Update later to pull this from the TTC website instead of hardcode by downloading the html as an xml file dynamically and parse using the xpath
        //For demo this will be hardcoded for time saving but will be in a modular structure for easy additions/changes



    }
    public String createResultURL(String searchType, String item,String category, String type, String trait, String quality,
                                  String LevelMin, String WritVoucherMin, String quantityMin, String quantityMax, String priceMin, String priceMax, String champion, String LevelMax, String WritVoucherMax)
    {
        //Create result url - Come back later to make this more modular by looping through options by class.getDeclaredVariables
        // Upon update gather variables in order on page, store in correct order, create url string using for loop
        String url = "https://us.tamrieltradecentre.com/pc/Trade/SearchResult?ItemID=&SearchType=";
        url = url + searchType + "&ItemNamePattern="+ item + "&ItemCategory1ID="+ category + "&ItemCategory2ID="+ type + "&ItemTraitID="+ trait +"&ItemQualityID="+ quality +"&IsChampionPoint=" + champion +
                "&LevelMin=" + LevelMin + "&LevelMax="+ LevelMax+"&MasterWritVoucherMin="+ WritVoucherMin+ "&MasterWritVoucherMax="+ WritVoucherMax + "&AmountMin=" + quantityMin + "&AmountMax=" + quantityMax +
                "&PriceMin=" + priceMin + "&PriceMax=" + priceMax;



        return url;

    }




}

